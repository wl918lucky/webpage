# webpage

<p align="center">
 <a target="_blank" href="LICENSE">
  <img src="https://img.shields.io/:License-Apache2.0-blue.svg">
 </a>

 
<br/>
<img src="https://img.shields.io/badge/vue-brightgreen.svg??style=flat-square">
<img src="https://img.shields.io/badge/webpack-brightgreen.svg?style=flat">

</p>


#### 安装项目依赖

```
npm install
```

#### 编译和热重载开发

```
npm run serve
```

#### 编译和简化生产

```
npm run build
```

#### 检查和修复文件

```
npm run lint
```

#### 请参阅配置参考

See [Configuration Reference](https://cli.vuejs.org/config/).
electron:build
