import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './assets/css/global.css'
import {Router} from "./router"
import axios from 'axios'


import './mock/mockData.js'
axios.defaults.baseURL='http://localhost:8080'

const app = createApp(App);
app.use(Router)
app.use(ElementPlus)
app.mount('#app')
