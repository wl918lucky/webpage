import Mock from 'mockjs'

//延时 400s 请求到数据
Mock.setup({
    timeout: 400
})

//延时 200-600 毫秒请求到数据
Mock.setup({
    timeout: '200-600'
})


// export const lineData = Mock.mock({
//     'list|5': [{
//       'id|+1': 1,
//       'name': '@cname',
//       'age|18-60': 1,
//       'email': '@email'
//     }]
//   });
//   export default lineData;



// const Mock = require('mockjs')

Mock.mock("http://localhost:8080/getTips", 'get', {
    message: "11111",
    status: 200,
    code: 200,
    "data" : {
        "list": [  
            { id: 1, name: '张三' },
            { id: 2, name: '李四' } ],
        "aaa":"aaaa",
        "abbbaa":"bbb",
        "aavvva":"aacccaa"
        // "date": "@now",
        // "string|1-6":"*",
        // "city": "@city"
    }
});

Mock.mock("'/getTips", 'get', {
    message: "2222",
    status: 200,
    code: 200,
    "data" : {
        "aaa":"aaaa",
        "abbbaa":"bbb",
        "aavvva":"aacccaa"
        // "date": "@now",
        // "string|1-6":"*",
        // "city": "@city"
    }
});