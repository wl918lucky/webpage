import { createRouter, createWebHashHistory } from 'vue-router';
import MainPage from '@/components/MainPage.vue';
import HelloWorld from '@/components/Home.vue';
import PictureShow from '@/components/PictureShow.vue';

const routes = [
    { path: '/', component: HelloWorld },
    { path: '/home', component: HelloWorld },
    { path: '/main', component: MainPage },
    { path: '/pic', component: PictureShow }

];

export const Router = createRouter({
    history: createWebHashHistory(),
    routes,
});

// export default router