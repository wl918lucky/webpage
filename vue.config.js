const { defineConfig } = require('@vue/cli-service')
// const Mock = require('mockjs')

module.exports = defineConfig({
  publicPath: process.env.NODE_ENV === "production" ? "/webpage/" : "./",
  transpileDependencies: true,
  // 关闭eslint校验
  lintOnSave: false
})
